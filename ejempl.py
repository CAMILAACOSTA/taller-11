import random

class Dado:

    def lanzarDado(self):
        self.valor=random.randit(1,6)

    def obtenerValor(self):
        return self.valor

class JuegDados:

    def __init__(self):
        self.dado1= Dado()
        self.dado2= Dado()
        self.dado3= Dado()

    def calcularResultado(self):
        self.resultado= self.dado1.obtenerValor()+self.dado2.obtenerValor()+self.dado3.obtenerValor()
        return self.resultado


class JuegoPremio(JuegDados):

    def calcularPremio(self):
        if self.resultado==7:
            self.premio=100

        else:
            self.premio=-100    

myJuego=JuegoPremio()
myJuego.jugar()
print(myJuego.dado1.obtenerValor())
print(myJuego.dado2.obtenerValor())
print(myJuego.dado3.obtenerValor())
print(myJuego.calcularResultado())
print(myJuego.calcularPremio())

